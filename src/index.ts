/**
 * @param func
 * @param wait
 */
export default function debounce(func: () => void, wait: number) {
    let timeout: any;
    return function() {
        let context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            timeout = null;
            func.apply(context, args);
        }, wait);
    };
}

